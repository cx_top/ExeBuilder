package exebuilder.controller;

import exebuilder.core.Controllers;
import exebuilder.data.AppConfig;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class EndController implements Initializable {

  private static final Logger logger = LogManager.getLogger();
  public TextArea buildInfoArea;


  public void showBuildInfo() {
    buildInfoArea.clear();
    var appConfig = AppConfig.getInstance();
    buildInfoArea.appendText("程序名称：" + appConfig.getAppName() + "\n");
    buildInfoArea.appendText("项目输出目录：" + appConfig.getOutputPath() + "\n");
    buildInfoArea.appendText("JDK版本：" + appConfig.getJdkVersion() + "\n");
    buildInfoArea.appendText("JDK目录：" + appConfig.getJdkPath() + "\n");
    buildInfoArea.appendText("源文件目录：" + appConfig.getSourcePath() + "\n");
    buildInfoArea.appendText("可执行Jar文件：" + appConfig.getMainJarFile() + "\n");
    buildInfoArea.appendText("lib文件目录：" + appConfig.getLibPath() + "\n");
    buildInfoArea.appendText("程序图标：" + appConfig.getIconPath() + "\n");
    buildInfoArea.appendText("版本信息：" + appConfig.getVersion() + "\n");
    buildInfoArea.appendText("版权信息：" + appConfig.getCopyright() + "\n");
    buildInfoArea.appendText("点击开始执行...\n");

  }

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    Controllers.add("end", this);
  }


}
